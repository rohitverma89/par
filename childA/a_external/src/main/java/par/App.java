package par;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * Hello world!
 */
@SpringBootApplication
public class App implements CommandLineRunner {

    @Resource
    private AExternal aExternal;

    @Resource
    private AInternal aInternal;


    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
    }

    public void run(String... strings) throws Exception {
        System.out.println(aExternal.getDef());
        System.out.println(aInternal.getDef());
    }
}
